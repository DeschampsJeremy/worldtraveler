export const environment = {
  production: false,

  //News
  newsApiKey: "__MY_KEY__",
  newsUrl: "https://newsapi.org/v2/everything",

  //Weather
  weatherApiKey: '__MY_KEY__',
  weatherUrl: 'http://api.openweathermap.org/data/2.5/forecast',

  //Point of interest
  poiUrl: 'https://overpass-api.de/api/interpreter',

  //Geocoding
  geocodingUrl: 'https://api.opencagedata.com/geocode/v1',
  geocodingApi: '__MY_KEY__',
};
