export const config = {
    title: "WorldTraveler",
    version: "1.0.0",
    owner: "Jeremy Deschamps",
    contact: "contact@jddev.net",
    sources: [
        "Flaticon",
        "OpenStreetMap",
        "NewsAPI",
        "OpenWeatherMap",
    ],
    repoUrl: "https://gitlab.com/DeschampsJeremy/worldtraveler",
}