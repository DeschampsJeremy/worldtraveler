import { Component, OnDestroy, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {

  gps: any = {};

  constructor(
    private geolocation: Geolocation,
  ) { }

  ngOnInit() {
    setInterval(() => {
      this.getGps();
    }, 1000);
  }
  ngOnDestroy() {
  }

  //Save GPS on local storage
  saveGPS() {
    localStorage.setItem("GPSs", JSON.stringify(this.gps));
  }

  //Get GPS
  getGps() {
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(
      (position) => {
        if ("coords" in position) {
          this.gps.isGPSError = false;
          this.gps.date = new Date();
          this.gps.lat = (position.coords.latitude) ? position.coords.latitude.toFixed(8) : null;
          this.gps.lng = (position.coords.longitude) ? position.coords.longitude.toFixed(8) : null;
          this.gps.accuracy = (position.coords.accuracy) ? position.coords.accuracy : null;
          this.gps.heading = (position.coords.heading) ? position.coords.heading : null;
          this.gps.speed = (position.coords.speed) ? position.coords.speed : null;
          this.saveGPS();
        } else {
          this.gps.isGPSError = true;
          this.saveGPS();
        }
      },
      (error) => {
        this.gps.isGPSError = true;
        this.saveGPS();
      });
  }

}
