import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PageEmergencyComponent } from './pages/page-emergency/page-emergency.component';
import { PageMapComponent } from './pages/page-map/page-map.component';
import { PageNewsComponent } from './pages/page-news/page-news.component';
import { PageSensorComponent } from './pages/page-sensor/page-sensor.component';
import { PageWeatherComponent } from './pages/page-weather/page-weather.component';

const routes: Routes = [
  { path: 'sensor', component: PageSensorComponent },
  { path: 'map', redirectTo: 'map/', pathMatch: 'full' },
  { path: 'map/:latLngTitle', component: PageMapComponent },
  { path: 'weather', component: PageWeatherComponent },
  { path: 'news', component: PageNewsComponent },
  { path: 'emergency', component: PageEmergencyComponent },
  { path: '', redirectTo: 'sensor', pathMatch: 'full' },
  { path: '**', redirectTo: 'sensor' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
