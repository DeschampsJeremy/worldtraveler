import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-component-header-modal',
  templateUrl: './component-header-modal.component.html',
  styleUrls: ['./component-header-modal.component.scss'],
})
export class ComponentHeaderModalComponent implements OnInit {
  @Input() title: string;
  
  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  //Dissim
  onDismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
