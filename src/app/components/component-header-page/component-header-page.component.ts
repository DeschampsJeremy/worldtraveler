import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalConfigComponent } from 'src/app/modals/modal-config/modal-config.component';
import { config } from '../../../datas/config';

@Component({
  selector: 'app-component-header-page',
  templateUrl: './component-header-page.component.html',
  styleUrls: ['./component-header-page.component.scss'],
})
export class ComponentHeaderPageComponent implements OnInit {
  config: any = config;

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  async modalConfig() {
    const modal = await this.modalController.create({
      component: ModalConfigComponent,
    });
    return await modal.present();
  }

}
