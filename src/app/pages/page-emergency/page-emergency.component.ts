import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ModalEmergencyListComponent } from 'src/app/modals/modal-emergency-list/modal-emergency-list.component';
import { EmergencyService } from 'src/app/services/emergency/emergency.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-page-emergency',
  templateUrl: './page-emergency.component.html',
  styleUrls: ['./page-emergency.component.scss'],
})
export class PageEmergencyComponent implements OnInit {
  httpSubcribe: Subscription;
  GPSSubcribe: Subscription;
  gps: any = {};
  phoneEmergency: any;
  address: string;

  constructor(
    private emergencyService: EmergencyService,
    private storageService: StorageService,
    private http: HttpClient,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.getGps();
  }
  ngOnDestroy() {
    if (this.GPSSubcribe) {
      this.GPSSubcribe.unsubscribe();
    }
    if (this.httpSubcribe) {
      this.httpSubcribe.unsubscribe();
    }
  }

  //Get GPS
  getGps() {
    this.GPSSubcribe = this.storageService.watch("GPSs").subscribe(
      (response) => {
        this.gps = response;
        if (!this.phoneEmergency) {
          this.getInvertGeocode();
        }
      }
    );
  }

  //Get Invert Geocode
  getInvertGeocode() {
    const url = environment.geocodingUrl + "/json?q=" + this.gps.lat + "+" + this.gps.lng + "&key=" + environment.geocodingApi;
    this.httpSubcribe = this.http.get(url).subscribe(
      (response) => {
        const countryISOCode = response['results'][0].components['ISO_3166-1_alpha-2'];
        this.phoneEmergency = this.emergencyService.getEmergencyCalls(countryISOCode);
        this.address = response['results'][0].formatted;
      },
      (error) => {
        this.phoneEmergency = null;
        this.address = null;
      }
    );
  }

  //Modal param
  async modalParam() {
    const modal = await this.modalController.create({
      component: ModalEmergencyListComponent,
    });
    return await modal.present();
  }
}
