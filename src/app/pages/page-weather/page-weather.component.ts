import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ToastController } from '@ionic/angular';
import { MapService } from 'src/app/services/map/map.service';

@Component({
  selector: 'app-page-weather',
  templateUrl: './page-weather.component.html',
  styleUrls: ['./page-weather.component.scss'],
})
export class PageWeatherComponent implements OnInit, OnDestroy {
  search: string;
  GPSSubcribe: Subscription;
  httpSubcribe: Subscription;
  weathers: any[] = null;
  city: any = null;
  gps: any = {};

  constructor(
    private http: HttpClient,
    private toastController: ToastController,
    private storageService: StorageService,
    private mapService: MapService,
  ) { }

  ngOnInit() {
    this.getGps();
  }
  ngOnDestroy() {
    if (this.GPSSubcribe) {
      this.GPSSubcribe.unsubscribe();
    }
    if (this.httpSubcribe) {
      this.httpSubcribe.unsubscribe();
    }
  }

  //Refresh
  doRefresh(event) {
    this.city = null;
    this.weathers = null;
    setTimeout(() => {
      event.target.complete();
      this.getWeathers();
    }, 2000);
  }

  //Get GPS
  getGps() {
    this.GPSSubcribe = this.storageService.watch("GPSs").subscribe(
      (response) => {
        this.gps = response;
        if (!this.weathers) {
          this.getWeathers();
        }
      }
    );
  }

  //Get news
  async getWeathers() {

    //Search
    const query = (this.search) ? '&q=' + this.search : '&lat=' + this.gps.lat + '&lon=' + this.gps.lng;
    const url: string = environment.weatherUrl + '?appid=' + environment.weatherApiKey + '&units=metric' + query;

    //Get HTTP request
    this.httpSubcribe = this.http.get(url).subscribe(
      (response) => {
        this.city = response['city'];
        this.weathers = response['list'];
        if (this.weathers.length < 1) {
          this.city = null;
          this.weathers = null;
        }
      },
      (error) => {
        this.city = null;
        this.weathers = null;
        this.alertError("Une erreur est survenue, merci de réessayer plus tard ...");
      });
  }

  //Wind orientation
  windOrientation(windDeg: number) {
    return this.mapService.orientationName(windDeg);
  }

  //Wind force
  windForce(windSpeed: number) {
    windSpeed = windSpeed * 3.6;
    if (windSpeed <= 1) {
      return 0;
    } else if (windSpeed <= 5) {
      return 1;
    } else if (windSpeed <= 11) {
      return 2;
    } else if (windSpeed <= 19) {
      return 3;
    } else if (windSpeed <= 28) {
      return 4;
    } else if (windSpeed <= 38) {
      return 5;
    } else if (windSpeed <= 49) {
      return 6;
    } else if (windSpeed <= 61) {
      return 7;
    } else if (windSpeed <= 74) {
      return 8;
    } else if (windSpeed <= 88) {
      return 9;
    } else if (windSpeed <= 102) {
      return 10;
    } else if (windSpeed <= 117) {
      return 11;
    } else if (windSpeed > 117) {
      return 12;
    }
  }

  //Toast
  toast: any;
  async alertError(message: string) {
    await this.toastController.create({
      message: message,
      position: 'middle',
      buttons: [
        {
          side: 'end',
          icon: 'close',
        }
      ]
    });
    this.toast.present();
  }

}
