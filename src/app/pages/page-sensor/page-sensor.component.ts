import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DeviceOrientation, DeviceOrientationCompassHeading } from '@ionic-native/device-orientation/ngx';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-page-sensor',
  templateUrl: './page-sensor.component.html',
  styleUrls: ['./page-sensor.component.scss'],
})
export class PageSensorComponent implements OnInit, OnDestroy {
  GPSSubcribe: Subscription;
  compassSubcribe: Subscription;
  isCompassError: boolean;
  gps: any = {};
  compass: any = {};

  constructor(
    private storageService: StorageService,
    private deviceOrientation: DeviceOrientation,
  ) { }

  ngOnInit() {
    this.getGps();
    this.getCompass();
  }
  ngOnDestroy() {
    if (this.GPSSubcribe) {
      this.GPSSubcribe.unsubscribe();
    }
    if (this.compassSubcribe) {
      this.compassSubcribe.unsubscribe();
    }
  }

  //Get GPS
  getGps() {
    this.GPSSubcribe = this.storageService.watch("GPSs").subscribe(
      (response) => {
        this.gps = response;
      }
    );
  }

  //Get orientation
  getCompass() {
    this.compassSubcribe = this.deviceOrientation.watchHeading().subscribe(
      (orientation: DeviceOrientationCompassHeading) => {
        this.isCompassError = false;
        this.compass.heading = (orientation.magneticHeading) ? orientation.magneticHeading.toString() : null;
      },
      (error) => {
        this.isCompassError = true;
      });
  }
}

