import { Component, OnInit } from '@angular/core';
import { MapService } from 'src/app/services/map/map.service';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { ModalMapParamComponent } from 'src/app/modals/modal-map-param/modal-map-param.component';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-map',
  templateUrl: './page-map.component.html',
  styleUrls: ['./page-map.component.scss'],
})
export class PageMapComponent implements OnInit {
  guidanceLatLngTitle: string = this.activatedRoute.snapshot.params['latLngTitle'];
  guidanceMessage: any = {};

  distanceDisplayPOIInMeter: number = 100000;

  httpSubcribe: Subscription;
  GPSSubcribe: Subscription;

  gps: any = {};

  POIs: any;
  isCallPOI: boolean = false;
  lastUserLatLngCallPOI: any;

  isMapCenter: boolean = false;

  constructor(
    private http: HttpClient,
    private toastController: ToastController,
    private alertController: AlertController,
    private modalController: ModalController,
    private storageService: StorageService,
    private mapService: MapService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    const excludeTitles = [
      "Point d'eau",
      "Restaurant",
      "Réparation de véhicule",
      "Point de recharge pour véhicule",
      "Wifi",
      "Hopital",
      "Magasin",
      "Police",
    ];
    this.storageService.init(this.mapService.getPOIs(), "POIs", excludeTitles);
  }
  ngOnDestroy() {
    if (this.httpSubcribe) {
      this.httpSubcribe.unsubscribe();
    }
  }
  ionViewDidEnter() {
    this.mapService.init('map');
    this.getGps();
    if (this.isCallPOI) {
      this.displayPOI();
    }
  }
  ionViewWillLeave() {
    this.closeToast();
    this.mapService.remove();
    this.isMapCenter = false;
    if (this.GPSSubcribe) {
      this.GPSSubcribe.unsubscribe();
    }
  }

  //Get GPS
  getGps() {
    this.GPSSubcribe = this.storageService.watch("GPSs").subscribe(
      (response) => {
        this.gps = response;
        this.mapService.clearAllMarker();
        this.mapService.addMarker(this.mapService.getLatlng(this.gps.lat, this.gps.lng), false, "Ma position", "marker-red");
        if (!this.isMapCenter) {
          this.onCenterLocation();
        }
        if (!this.isCallPOI) {
          this.getPOIs();
        }
        if (this.lastUserLatLngCallPOI && this.mapService.getDistance(this.lastUserLatLngCallPOI, this.mapService.getLatlng(this.gps.lat, this.gps.lng)) > 25000) {
          this.getPOIs();
        }
        if (this.guidanceLatLngTitle) {
          const guidances = this.guidanceLatLngTitle.split("_");
          const guidanceLatLng = this.mapService.getLatlng(guidances[0], guidances[1])
          let distance = this.mapService.getDistance(guidanceLatLng, this.mapService.getLatlng(this.gps.lat, this.gps.lng));
          let distanceStr = (distance > 1000) ? (distance / 1000).toFixed(2) + " km" : distance + " m";
          const direction = this.mapService.getDirection(this.mapService.getLatlng(this.gps.lat, this.gps.lng), guidanceLatLng);
          this.guidanceMessage = {
            title: guidances[2],
            distance: distanceStr,
            direction: this.mapService.orientationName(parseInt(direction)),
          };
        }
      }
    );
  }

  //Search
  async onSearch() {
    const alert = await this.alertController.create({
      header: 'Recherche',
      inputs: [
        {
          name: "search",
          placeholder: "Entrez une ville",
        }
      ],
      buttons: [
        {
          text: "Annuler",
          role: "cancel"
        },
        {
          text: 'Valider',
          handler: (inputs) => {
            this.getGeocode(inputs.search);
          }
        }
      ]
    });
    await alert.present();
  }

  //Get Geocode
  getGeocode(search: string) {
    this.httpSubcribe = this.http.get(environment.geocodingUrl + "/json?q=" + search + "&key=" + environment.geocodingApi).subscribe(
      (response) => {
        this.onCenterTo(this.mapService.getLatlng(response['results'][0].geometry.lat, response['results'][0].geometry.lng));
      },
      (error) => {
        this.alertError("Impossible de trouver cette ville, merci de vérifier l'orthographe ...");
      }
    );
  }

  //Get POIs
  getPOIs(latLng?: any) {
    this.isCallPOI = true;

    //Start loader
    this.initToast("Recherche des points d'intérêt en cours ...");

    //Unknow latLng
    if (!latLng) {
      latLng = this.mapService.getLatlng(this.gps.lat, this.gps.lng);
    }

    //Define bBox
    const bBox = this.mapService.getBoxLatLng(latLng, this.distanceDisplayPOIInMeter);
    const coordonateBox = bBox.minLat + "," + bBox.minLng + "," + bBox.maxLat + "," + bBox.maxLng;

    //Construc request
    let request = "";
    const storagePOIs = JSON.parse(localStorage.getItem("POIs"));
    request = '(';
    storagePOIs.forEach(POI => {
      request += '  node[' + POI.tag + '](' + coordonateBox + ');';
    });
    request += ')';

    //Get HTTP request
    const body = new HttpParams().set('data', '[out:json];' + request + ';out;')
    this.httpSubcribe = this.http.post(environment.poiUrl, body.toString(), {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe(
      (response) => {

        //Check : if empty init like array
        if (!this.POIs) {
          this.POIs = [];
        }

        //Check : doublon
        response['elements'].forEach(element => {
          let isFind = false;
          this.POIs.forEach(POI => {
            if (POI.id == element.id) {
              isFind = true;
            }
          });
          if (!isFind) {
            this.POIs.push(element);
          }
        });

        //Check : if empty set null
        if (this.POIs.length < 1) {
          this.POIs = null;
        } else {

          //Save last latLng
          this.lastUserLatLngCallPOI = this.mapService.getLatlng(this.gps.lat, this.gps.lng);

          //Display active markers
          this.displayPOI();

        }

        //Stop loader
        this.closeToast();
      },
      (error) => {
        this.closeToast();
        this.alertError("Impossible de trouver des points d'intérêt ici, merci de réessayer plus tard ...");
      });
  }

  //Display POI
  displayPOI() {
    this.mapService.clearAllPOIMarker();
    const activePOIs = this.storageService.getActives("POIs");
    this.POIs.forEach(POIdatas => {
      activePOIs.forEach(activePOI => {
        if (JSON.stringify(POIdatas.tags).includes(activePOI.tag.replace("=", ":"))) {
          const POIinfos = this.mapService.getPOIInfos(POIdatas);
          const latLng = this.mapService.getLatlng(POIdatas.lat, POIdatas.lon);
          const distanceMeter = this.mapService.getDistance(latLng, this.mapService.getLatlng(this.gps.lat, this.gps.lng));
          const distanceStr = (distanceMeter > 1000) ? (distanceMeter / 1000).toFixed(2) + " km" : distanceMeter + " m";
          //const link = "/map/" + POIdatas.lat + "_" + POIdatas.lon + "_" + POIinfos.title;
          //const title = "<b>" + POIinfos.title + "</b><br>Distance : " + distanceStr + '<br><a href="' + link + '">Lancer le guidage</a>';
          const title = "<b>" + POIinfos.title + "</b><br>Distance : " + distanceStr;
          this.mapService.addMarker(latLng, true, title, POIinfos.icon);
        }
      });
    });
  }

  //Alert
  async alertError(message: string) {
    const alert = await this.alertController.create({
      header: 'Erreur',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  //Toast
  toast: any;
  async initToast(message: string) {
    this.toast = await this.toastController.create({
      message: message,
      position: 'middle',
      buttons: [
        {
          side: 'end',
          icon: 'close',
        }
      ]
    });
    this.toast.present();
  }
  closeToast() {
    this.toast.dismiss();
  }

  //Center search
  onCenterTo(latLng: any) {
    this.isMapCenter = true;
    this.getPOIs(latLng);
    this.mapService.setCenter(latLng);
    this.mapService.setZoom(12);
  }

  //Center location
  onCenterLocation() {
    this.isMapCenter = true;
    const latLng = this.mapService.getLatlng(this.gps.lat, this.gps.lng);
    this.mapService.setCenter(latLng);
    setTimeout(() => {
      this.mapService.setZoom(15);
    }, 250);
  }

  //Modal param
  async modalParam() {
    const modal = await this.modalController.create({
      component: ModalMapParamComponent,
    });
    modal.onDidDismiss().then(() => {
      this.displayPOI();
    });
    return await modal.present();
  }

}
