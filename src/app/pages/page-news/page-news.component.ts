import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { AlertController, ModalController } from '@ionic/angular';
import { ModalNewsParamComponent } from 'src/app/modals/modal-news-param/modal-news-param.component';
import { NewsService } from 'src/app/services/news/news.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-page-news',
  templateUrl: './page-news.component.html',
  styleUrls: ['./page-news.component.scss'],
})
export class PageNewsComponent implements OnInit, OnDestroy {
  search: string;
  httpSubcribe: Subscription;
  news: any[] = null;

  constructor(
    private http: HttpClient,
    private newsService: NewsService,
    private storageService: StorageService,
    private alertController: AlertController,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.storageService.init(this.newsService.getSources(), "sources");
    this.getNews();
  }
  ngOnDestroy() {
    if (this.httpSubcribe) {
      this.httpSubcribe.unsubscribe();
    }
  }

  //Refresh
  doRefresh(event) {
    this.news = null;
    setTimeout(() => {
      event.target.complete();
      this.getNews();
    }, 2000);
  }

  //Get news
  getNews() {

    //Search
    const query = (this.search) ? this.search.toString() : "";

    //Construct request
    const page = 1;
    const pageSize = 100;
    const language = "fr";
    const storageSources = this.storageService.getActives("sources");
    let sources = [];
    storageSources.forEach(element => {
      sources.push(element.tag);
    });
    const sourceStr = sources.join(",");
    const url: string = environment.newsUrl + "?q=" + query + "&page=" + page + "&pageSize=" + pageSize + "&language=" + language + "&apiKey=" + environment.newsApiKey + "&sources=" + sourceStr;

    //Get HTTP request
    this.httpSubcribe = this.http.get(url).subscribe(
      (response) => {
        this.news = response['articles'];
        if (this.news.length < 1) {
          this.news = null;
        }
      },
      (error) => {
        this.news = null;
        this.alertError("Une erreur est survenue, merci de réessayer plus tard ...");
      });
  }

  //Link
  link(url: string) {
    window.open(url);
  }

  //Alert
  async alertError(message: string) {
    const alert = await this.alertController.create({
      header: 'Erreur',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  //Modal param
  async modalParam() {
    const modal = await this.modalController.create({
      component: ModalNewsParamComponent,
    });
    modal.onDidDismiss().then(() => {
      this.getNews();
    });
    return await modal.present();
  }
}
