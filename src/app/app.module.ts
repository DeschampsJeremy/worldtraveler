import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Module
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DeviceOrientation } from '@ionic-native/device-orientation/ngx';

//Component
import { ComponentFooterComponent } from './components/component-footer/component-footer.component';
import { ComponentHeaderModalComponent } from './components/component-header-modal/component-header-modal.component';
import { ComponentHeaderPageComponent } from './components/component-header-page/component-header-page.component';

//Modal
import { ModalConfigComponent } from './modals/modal-config/modal-config.component';
import { ModalNewsParamComponent } from './modals/modal-news-param/modal-news-param.component';
import { ModalMapParamComponent } from './modals/modal-map-param/modal-map-param.component';
import { ModalEmergencyListComponent } from './modals/modal-emergency-list/modal-emergency-list.component';

//Page
import { PageMapComponent } from './pages/page-map/page-map.component';
import { PageNewsComponent } from './pages/page-news/page-news.component';
import { PageWeatherComponent } from './pages/page-weather/page-weather.component';
import { PageSensorComponent } from './pages/page-sensor/page-sensor.component';
import { PageEmergencyComponent } from './pages/page-emergency/page-emergency.component';

@NgModule({
  declarations: [

    //Component
    ComponentHeaderPageComponent,
    ComponentHeaderModalComponent,
    ComponentFooterComponent,

    //Modal
    ModalConfigComponent,
    ModalNewsParamComponent,
    ModalMapParamComponent,
    ModalEmergencyListComponent,

    //Page
    PageMapComponent,
    PageSensorComponent,
    PageWeatherComponent,
    PageNewsComponent,
    PageEmergencyComponent,

    //System
    AppComponent
  ],
  entryComponents: [],
  imports: [
    FormsModule,
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(), 
    AppRoutingModule],
  providers: [
    DeviceOrientation,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
