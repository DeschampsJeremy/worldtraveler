import { Injectable } from '@angular/core';
import * as Leaflet from 'leaflet';

@Injectable({
  providedIn: 'root'
})
/**
 * Class for manage map operations
 * 
 * NOTE : the map div need to be more 100*100 px !
 * 
 * NOTE : On marker click event :
 * 
 * marker.on('click', (event) => {
 *    console.log("click");
 * });
 * 
 */
export class MapService {
  private earthRadius: number = 6378137

  map: Leaflet.Map;
  markers: Leaflet.Marker[] = [];
  markerPOIs: Leaflet.Marker[] = [];
  circles: Leaflet.Circle[] = [];

  constructor() { }

  ///////////////////////////////////////////////TOOLS

  /**
   * Get a LatLng object
   * @param lat String or number value
   * @param lng String or number value
   */
  getLatlng(lat: number | string, lng: number | string): Leaflet.LatLng {
    const newLat: number = (typeof lat === "string") ? parseFloat(lat) : lat;
    const newLng: number = (typeof lng === "string") ? parseFloat(lng) : lng;
    return new Leaflet.LatLng(newLat, newLng);
  }

  ///////////////////////////////////////////////MAP

  /**
   * Init map
   * @param divId Div id to display map 
   */
  init(divId: string): Leaflet.Map {
    this.map = new Leaflet.Map(divId, { zoomControl: false }).setView(this.getLatlng(0, 0), 8);

    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Datas &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> | Imagery &copy <a href="https://www.mapbox.com/">Mapbox</a>',
    }).addTo(this.map);
    new Leaflet.Control.Zoom({ position: 'bottomright' }).addTo(this.map);
    return this.map;
  }

  /**
   * Remove the map
   */
  remove(): void {
    this.map.remove();
  }

  ///////////////////////////////////////////////CENTER / ZOOM

  /**
   * Set map zoom
   * @param zoom 
   */
  setZoom(zoom: number): void {
    this.map.setZoom(zoom);
  }

  /**
   * Get map zoom
   * @param zoom 
   */
  getZoom(): number {
    return this.map.getZoom();
  }

  /**
   * Set zoom between two latLng
   * @param latLng1 
   * @param latLng2 
   */
  setZoomBetween(latLng1: Leaflet.LatLng, latLng2: Leaflet.LatLng): void {
    this.setZoom(this.rateZoom(latLng1, latLng2));
  }

  /**
   * Set map center
   * @param latLng 
   */
  setCenter(latLng: Leaflet.LatLng): void {
    this.map.setView(latLng, this.getZoom());
  }

  /**
   * Get map center
   */
  getCenter(): Leaflet.LatLng {
    return this.map.getCenter();
  }

  ///////////////////////////////////////////////MARKER

  /**
   * Add marker on map
   * @param latLng 
   * @param title 
   * @param icon 
   */
  addMarker(latLng: Leaflet.LatLng, isPOI: boolean, title?: string, icon?: string): Leaflet.Marker {
    if (!title) { title = null }
    if (!icon) {
      icon = "marker-red";
    }
    const leafletIcon = Leaflet.icon({
      iconUrl: 'assets/map/' + icon + '.png',
      iconSize: [50, 50],
    })

    const marker: Leaflet.Marker = Leaflet.marker(latLng, { icon: leafletIcon });
    marker.bindPopup(title);
    marker.addTo(this.map);
    if (isPOI) {
      this.markerPOIs.push(marker);
    } else {
      this.markers.push(marker);
    }
    return marker;
  }

  /**
   * Clear all markers
   */
  clearAllMarker() {
    this.markers.forEach(marker => {
      marker.remove();
    });
  }

  /**
   * Clear all POI markers
   */
  clearAllPOIMarker() {
    this.markerPOIs.forEach(marker => {
      marker.remove();
    });
  }

  ///////////////////////////////////////////////CIRCLE

  /**
   * Add circle on map
   * @param latLng
   * @param radius Value in Km 
   */
  addCircle(latLng: Leaflet.LatLng, radius: number | string): Leaflet.Circle {
    let newRadius: number = (typeof radius === "string") ? parseFloat(radius) : radius;
    newRadius *= 1000;
    const circle: Leaflet.Circle = Leaflet.circle(latLng, { radius: newRadius });
    this.map.addLayer(circle);
    this.circles.push(circle);
    return circle;
  }

  /**
   * Clear all circles
   */
  clearAllCircle() {
    this.circles.forEach(circle => {
      circle.remove();
    });
  }

  ///////////////////////////////////////////////Point of interest

  /**
   * Get POI name and icon
   */
  getPOIs(): any[] {
    return [
      { tag: '"amenity"="boat_rental"', title: "Location bateau", icon: "boat" },
      { tag: '"public_transport"="station"', title: "Transport publique", icon: "bus" },
      { tag: '"amenity"="taxi"', title: "Taxi", icon: "cab" },
      { tag: '"amenity"="police"', title: "Police", icon: "cop" },
      { tag: '"amenity"="charging_station"', title: "Point de recharge pour véhicule", icon: "electric" },
      { tag: '"amenity"="clinic"', title: "Hopital", icon: "hospital" },
      { tag: '"aeroway"="terminal"', title: "Aéroport", icon: "plane" },
      { tag: '"amenity"="car_rental"', title: "Location voiture", icon: "cab" },
      { tag: '"shop"="car_repair"', title: "Réparation de véhicule", icon: "repair" },
      { tag: '"amenity"="restaurant"', title: "Restaurant", icon: "restaurant" },
      { tag: '"shop"="supermarket"', title: "Magasin", icon: "shop" },
      { tag: '"public_transport"="station"', title: "Gare", icon: "train" },
      { tag: '"amenity"="drinking_water"', title: "Point d'eau", icon: "water" },
      { tag: '"internet_access"="wlan"', title: "Wifi", icon: "wifi" },
    ];
  }

  /**
   * Get POI info by response
   */
  getPOIInfos(POI: any): any {
    const allPOIs = this.getPOIs();
    for (let index = 0; index < allPOIs.length; index++) {
      if (JSON.stringify(POI.tags).includes(allPOIs[index].tag.replace("=", ":"))) {
        return allPOIs[index];
      }
    }
    return null;
  }

  ///////////////////////////////////////////////DISTANCE

  /**
   * Get a direction between two latLng
   * @param latLng1
   * @param latLng2 
   */
  getDirection(latLngOrigin: Leaflet.LatLng, latLngDestination: Leaflet.LatLng) {
    const lat1 = latLngOrigin.lat;
    const long1 = latLngOrigin.lng;
    const lat2 = latLngDestination.lat;
    const long2 = latLngDestination.lng;

    const dLon = (long2 - long1);
    const y = Math.sin(dLon) * Math.cos(lat2);
    const x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

    let brng = Math.atan2(y, x);
    brng = this.rad2deg(brng);
    brng = (brng + 360) % 360;

    return (brng).toFixed(0);
  }

  /**
   * Get a box in coordonate latLng by distance
   * @param latLng
   * @param distanceKm
   */
  getBoxLatLng(latLng: Leaflet.LatLng, distanceMeter: number): any {
    let earthPerimeterInMeter = 2 * Math.PI * this.earthRadius;
    let distanceMeterByDegreeLatitude = earthPerimeterInMeter / (90 * 2);
    let distanceMeterByDegreeLongitude = earthPerimeterInMeter / (180 * 2);
    let valToOperateLatitude = (distanceMeter / 2) / distanceMeterByDegreeLatitude;
    let valToOperateLongitude = (distanceMeter / 2) / distanceMeterByDegreeLongitude;
    let minLat = latLng.lat - valToOperateLatitude;
    let minLng = latLng.lng - valToOperateLongitude;
    let maxLat = latLng.lat + valToOperateLatitude;
    let maxLng = latLng.lng + valToOperateLongitude;
    return {
      minLat: minLat.toFixed(8),
      minLng: minLng.toFixed(8),
      maxLat: maxLat.toFixed(8),
      maxLng: maxLng.toFixed(8),
    }
  }

  /**
   * Get distance (in meters) between two latLng
   * @param latLng1 
   * @param latLng2 
   */
  getDistance(latLng1: Leaflet.LatLng, latLng2: Leaflet.LatLng): number {
    const $rlo1 = this.deg2rad(latLng1.lng);
    const $rla1 = this.deg2rad(latLng1.lat);
    const $rlo2 = this.deg2rad(latLng2.lng);
    const $rla2 = this.deg2rad(latLng2.lat);
    const $dlo = ($rlo2 - $rlo1) / 2;
    const $dla = ($rla2 - $rla1) / 2;
    const $a = (Math.sin($dla) * Math.sin($dla)) + Math.cos($rla1) * Math.cos($rla2) * (Math.sin($dlo) * Math.sin($dlo));
    const $d = 2 * Math.atan2(Math.sqrt($a), Math.sqrt(1 - $a));
    return Math.round(this.earthRadius * $d);
  }

  ///////////////////////////////////////////////ORIENTATION

  /**
   * Get orientation name
   * @param deg 
   */
  orientationName(deg: number): string {
    let cap: string;
    if (deg < 22.5 || deg >= 337.5) {
      cap = "N"
    } else if (deg >= 22.5 && deg < 67.5) {
      cap = "N-E"
    } else if (deg >= 67.5 && deg < 112.5) {
      cap = "E"
    } else if (deg >= 112.5 && deg < 157.5) {
      cap = "S-E"
    } else if (deg >= 157.5 && deg < 202.5) {
      cap = "S"
    } else if (deg >= 202.5 && deg < 247.5) {
      cap = "S-O"
    } else if (deg >= 247.5 && deg < 292.5) {
      cap = "O"
    } else if (deg >= 292.5 && deg < 337.5) {
      cap = "N-O"
    }
    return cap;
  }

  ///////////////////////////////////////////////PRIVATE

  /**
   * Convert rad to degrees
   * @param radian 
   */
  private rad2deg(radian: any): number {
    return radian * (180 / Math.PI);
  }

  /**
   * Convert degree to rad
   * @param degrees 
   */
  private deg2rad(degrees: any): number {
    return degrees * (Math.PI / 180);
  }

  /**
   * Rate optimal zoom between two latLng
   * @param latLng1 
   * @param latLng2 
   */
  private rateZoom(latLng1: Leaflet.LatLng, latLng2: Leaflet.LatLng): number {
    const distance = this.getDistance(latLng1, latLng2);
    const scale = distance / 500;
    const zoomLevel = (16 - Math.log(scale) / Math.log(2));
    let zoom = Math.floor(zoomLevel) - 1;
    if (zoom > 20) { zoom = 20 };
    if (zoom < 1) { zoom = 1 };
    return zoom;
  }
}
