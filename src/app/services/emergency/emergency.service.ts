import { Injectable } from '@angular/core';
import { phoneEmergencies } from '../../../datas/phoneEmergencies.js';

@Injectable({
  providedIn: 'root'
})
export class EmergencyService {

  constructor() { }

  getEmergencies() {
    return phoneEmergencies;
  }

  getEmergencyCalls(countryISOCode: string) {
    for (let index = 0; index < phoneEmergencies.length; index++) {
      if (phoneEmergencies[index].Country.ISOCode == countryISOCode) {
        return phoneEmergencies[index];
      }
    }
  }
}
