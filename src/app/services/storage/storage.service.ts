import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  init(allElements: any[], storageName: string, excludeTitles?: string[]): void {

    //Add new
    let newStorages = [];
    if (!localStorage.getItem(storageName)) {
      allElements.forEach(element => {
        let isExclude = false;
        if (excludeTitles) {
          if (excludeTitles.includes(element.title)) {
            isExclude = true;
          }
        }
        if (!isExclude) {
          element['active'] = true;
        } else {
          element['active'] = false;
        }
        newStorages.push(element);
      });
      localStorage.setItem(storageName, JSON.stringify(newStorages));
    }

    //Check update 
    let checkStorages = [];
    let selectStorages = JSON.parse(localStorage.getItem(storageName));
    selectStorages.forEach(element => {
      checkStorages.push({
        tag: element.tag,
        title: element.title,
        icon: element.icon,
      });
    });
    let allStorages = [];
    allElements.forEach(element => {
      allStorages.push({
        tag: element.tag,
        title: element.title,
        icon: element.icon,
      });
    });
    if (JSON.stringify(checkStorages) != JSON.stringify(allStorages)) {
      localStorage.removeItem(storageName);
      this.init(allElements, storageName);
    }
  }

  getActives(storageName: string): any[] {
    const saveStorages = JSON.parse(localStorage.getItem(storageName));
    let newStorages = [];
    saveStorages.forEach(element => {
      if (element.active) {
        newStorages.push(element);
      }
    });
    return newStorages;
  }

  watch(storageName: string): Observable<any> {
    return new Observable((observer) => {
      setInterval(() => {
        observer.next(JSON.parse(localStorage.getItem(storageName)));
      }, 1000)
    })
  }

}
