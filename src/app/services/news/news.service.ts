import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor() { }

  getSources() {
    return [
      { tag: "abc-news", title: "ABC News", icon: null },
      { tag: "aftenposten", title: "Aftenposten", icon: null },
      { tag: "al-jazeera-english", title: "Al-jazeera", icon: null },
      { tag: "ansa", title: "Ansa", icon: null },
      { tag: "argaam", title: "Argaam", icon: null },
      { tag: "ars-technica", title: "Ars-technica", icon: null },
      { tag: "ary-news", title: "Ary news", icon: null },
      { tag: "associated-press", title: "Associated press", icon: null },
      { tag: "axios", title: "Axios", icon: null },
      { tag: "bbc-news", title: "BBC news", icon: null },
      { tag: "bbc-sport", title: "BBC sport", icon: null },
      { tag: "bleacher-report", title: "Bleacher report", icon: null },
      { tag: "bloomberg", title: "Bloomberg", icon: null },
      { tag: "breitbart-news", title: "Breitbart news", icon: null },
      { tag: "cbc-news", title: "CBC news", icon: null },
      { tag: "cbs-news", title: "CBS news", icon: null },
      { tag: "cnbc", title: "CNBC", icon: null },
      { tag: "cnn", title: "CNN", icon: null },
      { tag: "daily-mail", title: "Daily mail", icon: null },
      { tag: "die-zeit", title: "Die zeit", icon: null },
      { tag: "el-mundo", title: "El mundo", icon: null },
      { tag: "espn", title: "ESPN", icon: null },
      { tag: "focus", title: "Focus", icon: null },
      { tag: "espn", title: "ESPN", icon: null },
      { tag: "football-italia", title: "Football italia", icon: null },
      { tag: "fortune", title: "Fortune", icon: null },
      { tag: "fox-news", title: "FOX news", icon: null },
      { tag: "fox-sports", title: "FOX sports", icon: null },
      { tag: "globo", title: "Globo", icon: null },
      { tag: "hacker-news", title: "Hacker news", icon: null },
      { tag: "ign", title: "IGN", icon: null },
      { tag: "independent", title: "Independent", icon: null },
      { tag: "la-gaceta", title: "La gaceta", icon: null },
      { tag: "la-nacion", title: "La nacion", icon: null },
      { tag: "la-repubblica", title: "La repubblica", icon: null },
      { tag: "le-monde", title: "Le monde", icon: null },
      { tag: "lenta", title: "Lenta", icon: null },
      { tag: "lequipe", title: "L'Équipe", icon: null },
      { tag: "les-echos", title: "Les echos", icon: null },
      { tag: "liberation", title: "Liberation", icon: null },
      { tag: "metro", title: "Metro", icon: null },
      { tag: "mtv-news", title: "MTV news", icon: null },
      { tag: "metro", title: "Metro", icon: null },
      { tag: "national-geographic", title: "Menational geographic", icon: null },
      { tag: "nbc-news", title: "NBC news", icon: null },
      { tag: "new-scientist", title: "New scientist", icon: null },
      { tag: "metro", title: "Metro", icon: null },
      { tag: "recode", title: "Recode", icon: null },
      { tag: "metro", title: "Metro", icon: null },
      { tag: "the-wall-street-journal", title: "The wall-street journal", icon: null },
      { tag: "the-washington-post", title: "The Washington post", icon: null },
      { tag: "time", title: "Time", icon: null },
      { tag: "ynet", title: "Ynet", icon: null },
    ];
  }
}
