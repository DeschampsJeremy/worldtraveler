import { Component, OnInit } from '@angular/core';
import { config } from '../../../datas/config';

@Component({
  selector: 'app-modal-config',
  templateUrl: './modal-config.component.html',
  styleUrls: ['./modal-config.component.scss'],
})
export class ModalConfigComponent implements OnInit {
  config: any = config;

  constructor() { }

  ngOnInit() { }

}
