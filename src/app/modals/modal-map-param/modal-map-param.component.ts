import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-map-param',
  templateUrl: './modal-map-param.component.html',
  styleUrls: ['./modal-map-param.component.scss'],
})
export class ModalMapParamComponent implements OnInit {
  selectPOIs: any[] = [];

  constructor() { }

  ngOnInit() {
    this.selectPOIs = JSON.parse(localStorage.getItem("POIs"));
  }

  onChange(index: number, $event: any) {
    this.selectPOIs[index]['active'] = $event.detail.checked;
    localStorage.setItem("POIs", JSON.stringify(this.selectPOIs));
  }
}