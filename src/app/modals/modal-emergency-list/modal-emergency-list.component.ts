import { Component, OnInit } from '@angular/core';
import { EmergencyService } from 'src/app/services/emergency/emergency.service';

@Component({
  selector: 'app-modal-emergency-list',
  templateUrl: './modal-emergency-list.component.html',
  styleUrls: ['./modal-emergency-list.component.scss'],
})
export class ModalEmergencyListComponent implements OnInit {
  emergencies: any[];

  constructor(
    private emergencyService: EmergencyService,
  ) { }

  ngOnInit() {
    this.emergencies = this.emergencyService.getEmergencies();
  }

}
