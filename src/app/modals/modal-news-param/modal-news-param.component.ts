import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-news-param',
  templateUrl: './modal-news-param.component.html',
  styleUrls: ['./modal-news-param.component.scss'],
})
export class ModalNewsParamComponent implements OnInit {
  selectSources: any[] = [];

  constructor() { }

  ngOnInit() {
    this.selectSources = JSON.parse(localStorage.getItem("sources"));
  }

  onChange(index: number, $event: any) {
    this.selectSources[index]['active'] = $event.detail.checked;
    localStorage.setItem("sources", JSON.stringify(this.selectSources));
  }
}
