[Home page](/wiki)
# CUSTOM APP

## Config

### Config

- Custom the `src\index.html` and `config.xml`

- Define in `src\datas\config.ts`

### Environments

- Define DEV in `src\environments\environment.ts`

- Define TEST in `src\environments\environment.test.env.ts`

- Define PROD in `src\environments\environment.prod.ts`

## Code

### Modules

- Define alls app modules in `src\app.module.ts`

### Icons

- Storage in `src\assets\icons`

- Use on `<ion-icon src="assets/icons/myCustomIcon.svg"></ion-icon>`

### Images

- In the `src\assets\images` folder

### Fonts

- Import in `src\index.html`

- Define in `src\theme\variables.scss`

### Style vars

- Define in `src\theme\variables.scss`

### Class style

- Global class (for all components) in `src\global.scss`

- Component class (for one component) in `src\components\__MY_COMPONENT__\__MY_COMPONENT__.page.scss`

### Pipes

- On the `src\pipes` folder

### Services

- On the `src\services` folder

### htaccess

- The `src\.htaccess` file