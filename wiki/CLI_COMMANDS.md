[Home page](/wiki)
# CLI COMMANDS

- Add a Component (code only for one page), in a CLI :
```
ionic g component __COMPONENT_NAME__  
```

- Add a Service (controllers code for all pages), in a CLI :
```
ionic g service __SERVICE_NAME__  
```

- Add a Pipe (views code for all pages), in a CLI :
```
ionic g pipe __PIPE_NAME__  
```

- Start local server and launch browser
```
npm run start
```

- Restart local server
```
npm run restart
```

- Create resources folder
```
npm run resources
```

- Update Icons and Splash screen
```
npm run up-icon
```

- Start browser cordova dev server
```
npm run start-dev-browser
```

- Start browser cordova prod server
```
npm run start-prod-browser
```

- Start android cordova dev server
```
npm run start-dev-android
```

- Start android cordova prod server
```
npm run start-prod-android
```

- Build a Web Browser dev version
```
npm run build-dev-browser
```

- Build a Web Browser test version
```
npm run build-test-browser
```

- Build a Web Browser prod version
```
npm run build-prod-browser
```

- Build a Android dev version
```
npm run build-dev-android
```

- Build a Android test version
```
npm run build-test-android
```

- Build a Android prod version
```
npm run build-prod-android
```

- Build an Android AAB file and an APK debug
```
npm run create-aab
```

- Build an Android Key file 
```
npm run keygen-android
```

- Sign a Android prod version
```
npm run sign-android
```

- Clean and copy the Web Browser build content
```
npm run copy-browser
```

- Clean and copy the Android build content
```
npm run copy-android
```

- Icon + Build : for DEV
```
npm run magic-build-dev
```

- Icon + Build + Sing : for TEST
```
npm run magic-build-test
```

- Icon + Build + Sing : for PROD
```
npm run magic-build-prod
```