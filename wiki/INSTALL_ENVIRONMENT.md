[Home page](/wiki)
# INSTALL ENVIRONMENT

- IDE : [Visual Studio Code](https://code.visualstudio.com/download)

- Universal CLI : [Git >= 2.21.0](https://git-scm.com/downloads), define your Gitlab / Github account in CLI :
```
git config --global user.name "UserName"
git config --global user.email email@example.com
```
- Javascript dependency : [JDK >= 8u211](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

- Javascript framework and dependencies manager: [NPM & Node >= 10.16.0](https://nodejs.org/en/) and restart your computer.

- Web Javascript framework : AngularCLI = 8.3.3, in a CLI :
```
npm install -g @angular/cli@8.3.3
```

- Hybrid Javascript dependency : Cordova = 9.0.0, in a CLI :
```
npm install -g cordova@9.0.0
npm install -g cordova-res
```

- Hybrid Javascript framework : Ionic = 5.2.3, in a CLI :
```
npm install -g ionic@5.2.3
npm install -g native-run
```

- Android Studio dependency : [GRADLE >= 5.5.1](https://gradle.org/releases/). Create a `C:\Gradle` folder and decompress the folder in it.

- Android compiler : [Android Studio & SDK >= 3.4](https://developer.android.com/studio#downloads). Launch Android Studio, go to `SDK Manager`, select `Android 11.0 (R) API 30` and `accept SDK licenses`. 

- Configure CLI with Android commands : find your `environment variables manager`. 
1) Add a system new variable `JAVA_HOME` value `C:\Program Files\Java\__MY_JDK_VERSION__\` and modify the system variable `PATH` to add `C:\Program Files\Java\__MY_JDK_VERSION__\bin\`. 
2) Add a system new variable `ANDROID_HOME` value `C:\Users\__MY_USER__\AppData\Local\Android\Sdk\`, `ANDROID_SDK_ROOT` value `C:\Users\__MY_USER__\AppData\Local\Android\Sdk`, modify the system variable `PATH` to add `C:\Users\__MY_USER__\AppData\Local\Android\Sdk\tools\` and `C:\Users\__MY_USER__\AppData\Local\Android\Sdk\platform-tools\`. 
3) Modify the system variable `PATH` to add `C:\Gradle\__MY_GRADLE_VERSION__\bin`. (WARNING : roots can be change on your system, check it !)

- Android emulator : launch Android Studio, on Configure button, select `AVD Manager`. Create a new virtual device, download and install the image system `Pie API 28`. Now you can click on the play button to your device for launch application on android system, this will take long time to start ...

- Unlock script command, open your Windows PowerShell in Administrator :
```
set-executionpolicy unrestricted
```
Submit your choice on run `o` command !

## Add dev tools

- All moderns browsers : [GoogleChrome](https://www.google.com/intl/fr_fr/chrome/), [Firefox](https://www.mozilla.org/fr/firefox/new/), [Opera](https://www.opera.com/fr/download), [Edge](https://www.microsoft.com/fr-fr/edge), Safari is only running on IOS !