[Home page](/wiki)
# LOCAL RUN

- Launch APP server, in the CLI :
```
npm run start
```
- Visit : [http://localhost:8100](http://localhost:8100)

- Use `Ctrl + C` to down server

- [OPTIONAL] Run it on Browser device, in the CLI :
```
npm run start-dev-browser
```
For debugging, press F12 to access to console debug.

- [OPTIONAL] Run it on Android virtual device, run your `Android Studio AVD Manager`, run your `Virtual Device` and push your app, in the CLI :
```
npm run start-dev-android
```
For debugging, go to the `Settings` menu on your device, select `About this phone`, press 7 times on the `Build number` to unlock `Developer mode`. Return to your computer, open the Chrome browser, go to [Developer Tools](chrome://inspect) and select your application, press `Inspect` to access the debug console. **This requires a lot of resources, it's better to use a real device !**

- [OPTIONAL] Run it on Android real device, go to the `Settings` menu on your device, select `About this phone`, press 7 times on the `Build number` to unlock `Developer mode`. Go back to your `Settings` menu and select `Developer options`, then activate `USB debugging`, next connect your device by `USB` and select on your device `USB for transfert files`, then push your app, in the CLI :
```
npm run start-dev-android
```
For debugging, return to your computer, open the Chrome browser, go to [Developer Tools](chrome://inspect) and select your application, press `Inspect` to access the debug console.