[Home page](/wiki)
# DEPLOY

## Build

- Build a version, in a CLI :
```
npm run magic-build-dev
```
OR
```
npm run magic-build-test
```
OR
```
npm run magic-build-prod
```

- Deploy your webapp : you need to copy your `build\webapp` folder on the `www` online folder

- Deploy your android app : you need to copy your the `build\androidapp\app-android.aab` on [Google Play](https://play.google.com/apps/publish/) and you can test it with the `build\androidapp\app-debug.apk` file