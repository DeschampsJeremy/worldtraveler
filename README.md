# WorldTraveler

By [Jeremy Deschamps](https://www.jddev.net)

Traveler app, map view, weather, news, emergency contact.

## Install & Contrib

- Make sure your environment is ready, consult : [Install environment](/wiki/INSTALL_ENVIRONMENT.md)

- Download the repository, in a CLI :
```
git clone https://gitlab.com/DeschampsJeremy/worldtraveler.git
cd worldTraveler
```

- Install dependencies :
```
npm i
ionic cordova prepare
```

- Custom your environment : create the `src\environments\environment.ts`, `src\environments\environment.prod.ts` and `src\environments\environment.test.env.ts` files, use the `src\environments\environment.example.ts` as an example. Next run on CLI

- Create a key to sign your app and define a password (keep it):
```
npm run keygen-android
```

Push only on the `staging` branch.

## Docs

- [Local run](/wiki/LOCAL_RUN.md)

- [Custom App](/wiki/CUSTOM_APP.md)

- [CLI commands](/wiki/CLI_COMMANDS.md)

- [Deploy](/wiki/DEPLOY.md)

## Dev note

- `!!!` in code this a mark indicate of something was wrong here

- Consult the `NOTE` file for check not solved known issues

## Includes

- HTML 5 : [https://www.w3schools.com/html/default.asp](https://www.w3schools.com/html/default.asp)

- CSS 3 : [https://www.w3schools.com/css/default.asp](https://www.w3schools.com/css/default.asp)

- JS 6 : [https://www.w3schools.com/js/default.asp](https://www.w3schools.com/js/default.asp)

- NodeJs 8 (JS Framework) : [https://nodejs.org/dist/latest-v12.x/docs/api/](https://nodejs.org/dist/latest-v12.x/docs/api/)

- Angular CLI 8 (Node Framework) : [https://angular.io/docs](https://angular.io/docs)

- Ionic 5 (Node Mobil Framework) : [https://ionicframework.com/docs](https://ionicframework.com/docs)
